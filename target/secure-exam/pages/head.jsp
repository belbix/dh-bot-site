<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<html>
<body>
<table style="width: auto; margin-bottom: 4pt; margin-left: 4pt;">
    <tr>

        <td style="width: 170pt"><a href="<c:url value="/"/>" class="btn-edit btn-primary" role="button" style="width: 150pt">Главная</a></td>
        <td style="width: 170pt"><a href="<c:url value="/tablecart"/>" class="btn-edit btn-primary" role="button" style="width: 150pt">База данных</a></td>
        <td style="width: 170pt"><a href="<c:url value="/tablework"/>" class="btn-edit btn-primary" role="button" style="width: 150pt">Рабочая по ИП</a></td>
        <c:if test="${param.select_menu == 'work_org'}">
            <td style="width: 170pt"><a href="<c:url value="/work_org"/>" class="btn-edit btn-success" role="button" style="width: 150pt">Рабочая по ООО</a></td>
        </c:if>
        <c:if test="${param.select_menu != 'work_org'}">
            <td style="width: 170pt"><a href="<c:url value="/work_org"/>" class="btn-edit btn-primary" role="button" style="width: 150pt">Рабочая по ООО</a></td>
        </c:if>
        <td style="width: 170pt"><a href="<c:url value="/svodka"/>" class="btn-edit btn-primary" role="button" style="width: 150pt">Сводка</a></td>
        <td style="width: 170pt"><a href="<c:url value="/kesh"/>" class="btn-edit btn-primary" role="button" style="width: 150pt">Кэш</a></td>
    </tr>
</table>
</body>
</html>

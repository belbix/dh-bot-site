function orgList(id_list, value, vid_oper) {
    var orgListHTML = "";
    var orgAllHTML = "";
    var clientAllHTML = "";
    var ipAllCartHTML = "";
    var ipAllSchetHTML = "";



    if (value == "1" || value == "8") {
        for (var i = 0; i < orgAll.length; i++) {
            orgAllHTML = orgAllHTML +
                "<option value='" + orgAll[i].id + "_org' label='" + orgAll[i].org + "(" + orgAll[i].balans + ")' id='org" + id_list + "Option" + i + "'/>";
        }
    }


    for (var i = 0; i < ipAll.length; i++) {
        var ip_one = ipAll[i];
        if (ip_one.vid_scheta == "Счет карты" && ip_one.ban == "Работает" && (value == "9" || value == "4")) {
            ipAllCartHTML = ipAllCartHTML +
                "<option value='" + ip_one.id + "_ip' label='" + ip_one.name + "(" + ip_one.balans + ")' id='org" + id_list + "Option" + i + "'/>";
        }
        if (ip_one.vid_scheta == "Расчетный счет ИП" && ip_one.ban == "Работает" && (value == "2" || value == "5")) {
            ipAllSchetHTML = ipAllSchetHTML +
                "<option value='" + ip_one.id + "_ip' label='" + ip_one.name + "(" + ip_one.balans + ")' id='org" + id_list + "Option" + i + "'/>";
        }
    }
    if (value == "3" || value == "6") {
        for (var i = 0; i < clientAll.length; i++) {
            clientAllHTML = clientAllHTML +
                "<option value='" + clientAll[i].id + "_cl' label='" + clientAll[i].org + "' id='client" + id_list + "Option" + i + "'/>";
        }
    }

    if (vid_oper == "post") {
        document.getElementById("orgListPost" + id_list).innerHTML =
            "<select id='post" + id_list + "Otkyda'>" +
            "<option value='' label='---Откуда---'></option>" +
            orgAllHTML +
            "<option value='' label='------'></option>" +
            ipAllSchetHTML +
            "<option value='' label='------'></option>" +
            ipAllCartHTML +
            "<option value='' label='------'></option>" +
            clientAllHTML +
            "</select>";
    }
    if (vid_oper == "rash") {
        document.getElementById("orgListRash" + id_list).innerHTML =
            "<select id='rash" + id_list + "Kyda'>" +
            "<option value='' label='---Куда---'></option>" +
            orgAllHTML +
            "<option value='' label='------'></option>" +
            ipAllSchetHTML +
            "<option value='' label='------'></option>" +
            ipAllCartHTML +
            "<option value='' label='------'></option>" +
            clientAllHTML +
            "</select>";
    }
  //  return orgListHTML;
}
function oper($val, $id, $bd) {
    if ($val == "") {
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "none";
            document.getElementById("prihod_id" + $id).style.display = "none";
            document.getElementById("kom_id" + $id).style.display = "none";
            document.getElementById("rashod_id" + $id).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "1") { //postyplenie ot nashih OOO
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "block";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "none";
        }

        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "2") { //Поступление от ИП
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "block";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "3") { //   Постуление из внешки
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "block";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "4") { // Отправка на карту
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "none";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "block";
        }
    }

    if ($val == "5") { // Отправка нашему ИП
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "none";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "6") { // Отправка на внешку
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "none";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "7") { //Комиссия
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "none";
            document.getElementById("prihod_id" + $id).style.display = "none";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "8") { //Отправка нашей ООО
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "none";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "block";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "none";
        }
    }

    if ($val == "9") { //Поступление с карты
        if ($bd == 1) {
            document.getElementById("otkyda_id" + $id).style.display = "block";
            document.getElementById("prihod_id" + $id).style.display = "block";
            document.getElementById("kom_id" + $id).style.display = "block";
            document.getElementById("rashod_id" + $id).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_org_id" +$id)); i++) {
            document.getElementById("org_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_cl_id" +$id)); i++) {
            document.getElementById("cl_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ips_id" +$id)); i++) {
            document.getElementById("ips_id" + $id + "_" + i).style.display = "none";
        }
        for (var i = 1; i <= parseInt(window.localStorage.getItem("st_ipk_id" +$id)); i++) {
            document.getElementById("ipk_id" + $id + "_" + i).style.display = "block";
        }
    }
}

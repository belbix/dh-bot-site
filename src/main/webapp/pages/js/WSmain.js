var stompClient = null;
var countOrg = 0;
var countIp = 0;
var countClient = 0;
var countPost = 0;
var operacCount = 0;
var operacAll = [];
var postAll = [];
var orgAll = [];
var ipAll = [];
var clientAll = [];
var countLoad = 0;
var postCount = 0;
var postHTML = "none";
var rashHTML = "none";
var sumCount = 0;
var loadPost = false;
var countLoadOrg = 0;
var countLoadOper = 0;
var countLoadIp = 0;
var countLoadCl = 0;
var countLoadPost = 0;
var countHTML = "";
var now = "";
createDate();


function createDate() {
    var currentTime = new Date();
    var currentDay = currentTime.getDate();
    var currentMonth = currentTime.getMonth();
    var currentYear = currentTime.getUTCFullYear();
    if (currentDay < 10) {
        currentDay = "0"+currentDay;
    }
    if (currentMonth+1 < 10) {
        currentMonth = "0"+(currentMonth+1);
    }
    now = currentYear+"-"+currentMonth+"-"+currentDay;
}

function connect() {
    countLoad = 0;
    stompClient = null;
    countOrg = 0;
    operacCount = 0;
    operacAll = [];
    orgAll = [];
    orgBalansAll = [];
    operacHTML = "";
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/msg', function (msg) {
            income(msg);
        });
        setOrg();
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}


function income(message) {

    var msg = JSON.parse(message.body);

    if (msg.msgId == "get_oper_count") {
        operacCount = msg.msg;
        for (var i = 1; i <= operacCount; i++) {
            getOperac(i);
        }
    }

    if (msg.msgId == "get_operac_name") {
        countLoadOper++;
        countLoad++;
        operacAll.push([msg.countId, msg.msg]);
        if (msg.countId == operacCount) {
            getIpCount();
            getOrgCount();
            getClientCount();
        }
    }

    if (msg.msgId == "get_org_count") {
        countOrg = msg.msg;
        for (var i = 1; i <= countOrg; i++) {
            getOrg(i);
        }
    }

    if (msg.msgId == "get_ip_count") {
        countIp = msg.msg;
        for (var i = 1; i <= countIp; i++) {
            getIpName(i);
        }
    }

    if (msg.msgId == "get_client_count") {
        countClient = msg.msg;
        for (var i = 1; i <= countClient; i++) {
            getClient(i);
        }
    }

    if (msg.msgId == "get_ip") {
        countLoadIp++;
        countLoad++;
        ipAll.push(msg);
    }

    if (msg.msgId == "get_org") {

        getPost("",msg.id); // запрашиваем поступления для этой организации
        countLoadOrg++;
        countLoad++;
        orgAll.push(msg);
    }

    if (msg.msgId == "get_client") {
        countLoadCl++;
        countLoad++;
        clientAll.push(msg);
    }

    if (msg.msgId == "post_all") { //получаем сообщение с кол-вом поступлений от каждой организации отдельно
        if (countLoadOrg == countOrg) { //если организации прогрузились, то значит больше не будет
            loadPost = true;
        }
        if (countPost == 0) { var tmp = 0;} else { var tmp = countPost;}
        for (var i = tmp; i < msg.id; i++) { //запрашиваем каждое поступление отдельно
            getPostSend(i, msg.kyda);
        }
        countPost = parseInt(msg.id); //сколько сообщений ждем
    }

    if (msg.msgId == "get_post") {
        countLoadPost++;
        countLoad++;
        postAll.push(msg);
    }

    if (msg.msgId == "post_add") {
        postAll.push(msg);
        creatMainTable();
        document.getElementById("post" + msg.incomeMsg + "Btn").disabled = false;
        document.getElementById("rash" + msg.incomeMsg + "Btn").disabled = false;
        //alert("Поступление для " + orgAll[msg.incomeMsg-1].org + " запланировано!");
    }

    if (msg.msgId == "post_del") {
        postAll.splice(msg.incomeMsg,1);
        creatMainTable();
    }

    if (msg.msgId == "post_ex") {

        if (msg.vid_operacii == "1") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "2" || msg.vid_operacii == "9") {
            for (var i = 0; i < ipAll.length; i++) {
                if (ipAll[i].id == msg.otkyda) {
                    ipAll[i].balans = parseFloat(ipAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "8") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "4" || msg.vid_operacii == "5") {
            for (var i = 0; i < ipAll.length; i++) {
                if (ipAll[i].id == msg.kyda) {
                    ipAll[i].balans = parseFloat(ipAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "3") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "6" || msg.vid_operacii == "7" || msg.vid_operacii == "10") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
        }

        postAll[msg.incomeMsg].isp = "1";
        creatMainTable();
    }

    if (msg.msgId == "post_ex_back") {
        if (msg.vid_operacii == "1") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "2" || msg.vid_operacii == "9") {
            for (var i = 0; i < ipAll.length; i++) {
                if (ipAll[i].id == msg.otkyda) {
                    ipAll[i].balans = parseFloat(ipAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "8") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "4" || msg.vid_operacii == "5") {
            for (var i = 0; i < ipAll.length; i++) {
                if (ipAll[i].id == msg.kyda) {
                    ipAll[i].balans = parseFloat(ipAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "3") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.kyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) - parseFloat(msg.sumPost);
                }
            }
        }
        if (msg.vid_operacii == "6" || msg.vid_operacii == "7" || msg.vid_operacii == "10") {
            for (var i = 0; i < orgAll.length; i++) {
                if (orgAll[i].id == msg.otkyda) {
                    orgAll[i].balans = parseFloat(orgAll[i].balans) + parseFloat(msg.sumPost);
                }
            }
        }


        postAll[msg.incomeMsg].isp = "0";
        orgList(msg.id);
        creatMainTable();
    }


    sumCount = parseInt(countOrg) + parseInt(operacCount) + parseInt(countIp) + parseInt(countClient) + parseInt(countPost);

    if (countLoad == sumCount && countOrg != 0 && operacCount != 0 && countIp != 0 && countClient != 0 && loadPost == true) {
        creatMainTable();
    } else {
        initCount();
    }

    console.log("loooog=  "+ "Org = " + parseInt(countOrg)+ " operac = " + parseInt(operacCount)+ " ip = " + parseInt(countIp)+
        " cl = " + parseInt(countClient) + "post = " + parseInt(countPost) + " sum =" + sumCount + " count = " + countLoad);
}

function initCount(){
    countHTML = "";
    if (countOrg != 0){
        countHTML = countHTML + "Загрузка организаций " + parseInt(countLoadOrg/countOrg*100) + "%<br>";
    } else {
        countHTML = countHTML + "Загрузка организаций 0%<br>";
    }
    if (countIp != 0){
        countHTML = countHTML + "Загрузка ИП " + parseInt(countLoadIp/countIp*100) + "%<br>";
    } else {
        countHTML = countHTML + "Загрузка ИП 0%<br>";
    }
    if (operacCount != 0){
        countHTML = countHTML + "Загрузка Операций " + parseInt(countLoadOper/operacCount*100) + "%<br>";
    } else {
        countHTML = countHTML + "Загрузка Операций 0%<br>";
    }
    if (countClient != 0){
        countHTML = countHTML + "Загрузка Клиентов " + parseInt(countLoadCl/countClient*100) + "%<br>";
    } else {
        countHTML = countHTML + "Загрузка Клиентов 0%<br>";
    }
    if (countPost != 0){
        countHTML = countHTML + "Загрузка Поступлений " + parseInt(countLoadPost/countPost*100) + "%<br>";
    } else {
        countHTML = countHTML + "Загрузка Поступлений 0%<br>";
    }
    var tmp_count = 0;

    if (countOrg == 0 || operacCount == 0 || countIp == 0 || countClient == 0) {
        tmp_count = 1;
    } else {
        tmp_count = parseInt((countLoad/sumCount)*100);
    }

    document.getElementById("tableOrg").innerHTML = "<tr><td colspan='4'><div><div style='background:#00c300;" +
        "" +
        " height:30px; width:" + tmp_count + "%;'>" +
        "</div>" +
        "<div style='position: absolute; top:25%; left:35%'>"+countHTML+"</div>" +
        "</div></td></tr>";
}

function getOrg(count) {
    var id = "get_org";
    stompClient.send("/app/ws_org", {}, JSON.stringify({msg: count, msgId: id}));
}

function getBalans(count) {
    var id = "get_org_balans";
    stompClient.send("/app/ws", {}, JSON.stringify({msg: count, msgId: id}));
}

function getOrgCount() {
    var id = "get_org_count";
    stompClient.send("/app/ws", {}, JSON.stringify({msgId: id}));
}

function getAllOperac() {
    var id = "get_oper_count";
    stompClient.send("/app/ws", {}, JSON.stringify({msgId: id}));
}

function getOperac(count) {
    var id = "get_operac_name";
    stompClient.send("/app/ws", {}, JSON.stringify({msg: count, msgId: id}));
}

function getIpCount() {
    var id = "get_ip_count";
    stompClient.send("/app/ws", {}, JSON.stringify({msgId: id}));
}

function getIpName(count) {
    var id = "get_ip";
    stompClient.send("/app/ws_ip", {}, JSON.stringify({msg: count, msgId: id}));
}

function getClientCount() {
    var id = "get_client_count";
    stompClient.send("/app/ws", {}, JSON.stringify({msgId: id}));
}

function getClient(count) {
    var id = "get_client";
    stompClient.send("/app/ws_client", {}, JSON.stringify({msg: count, msgId: id}));
}

function getPost(otkyda_org, kyda_org) {
    if (otkyda_org != "") {
        var id = "get_post_otkyda";
    } else {
        var id = "get_post_kyda";
    }

    if (loadPost == true) { var first = 1 } else { var first = 0}

    stompClient.send("/app/ws_post", {}, JSON.stringify({msgId: id, otkyda:otkyda_org, kyda: kyda_org, msgCount:first}));
}

function getPostSend(i,kyda){
    var id = "get_post_send";
    stompClient.send("/app/ws_post_send", {}, JSON.stringify({msgId: id, msgCount:i, kyda:kyda}));
}

function setOrg() {
    getAllOperac();
}

function creatMainTable() {
    var tableHTML = "";
    var operacIncomeHTML = "";
    var operacOutHTML = "";

    orgAll.sort(function (a, b) {
        if (parseInt(a.id) < parseInt(b.id)) {
            return -1;
        }
        if (parseInt(a.id) > parseInt(b.id)) {
            return 1;
        }
        return 0;
    });
    operacAll.sort(function (a, b) {
        if (parseInt(a[0]) < parseInt(b[0])) {
            return -1;
        }
        if (parseInt(a[0]) > parseInt(b[0])) {
            return 1;
        }
        return 0;
    });
    ipAll.sort(function (a, b) {
        if (parseInt(a.id) < parseInt(b.id)) {
            return -1;
        }
        if (parseInt(a.id) > parseInt(b.id)) {
            return 1;
        }
        return 0;
    });

    for (var i = 0; i < operacAll.length; i++) {
        var operacName = operacAll[i][1];
        if (operacName.substring(0, 2) == "С ") {
            operacIncomeHTML = operacIncomeHTML + "<option value='" + operacAll[i][0] + "' label='" + operacAll[i][1] + "'></option>";
        }
    }
    for (var i = 0; i < operacAll.length; i++) {
        var operacName1 = operacAll[i][1];
        if (operacName1.substring(0, 2) == "На" || (operacName1.substring(0, 2) == "ЗП") || (operacName1.substring(0, 2) == "Ко")) {
            operacOutHTML = operacOutHTML + "<option value='" + operacAll[i][0] + "' label='" + operacAll[i][1] + "'></option>";
        }
    }
    for (var i = 1; i <= countOrg; i++) {


        tableHTML = tableHTML +
            "<tr>" +
            "<td>" +

            "<table class='table table-bordered'>" +
            "<tbody>" +
            "<tr class='info'>" +
            "<td>" +
            "<input type='date' value='"+now+"' id='post" + i + "Data'>" +
            "</td>" +
            "<td>" +
            "<select id='post" + i + "Operac' onchange='selectOperPost(value,"+i+")'>" +
            "<option value='' label='---Вид операции---'></option>" +
            operacIncomeHTML +
            "</select>" +
            "</td>" +
            "<td id='orgListPost"+orgAll[i - 1].id+"'>" +
            "</td>" +
            "<td>" +
            "<input id='post" + i + "Sum' placeholder='Сумма' cssClass='input-sm' cssStyle='width: 60pt'/>" +
            "</td>" +
            "<td>" +
            "<input id='post" + i + "Koment' placeholder='Комент' cssClass='input-sm' cssStyle='width: 60pt'/>" +
            "</td>" +
            "<td>" +
            "<input id='post" + i + "Btn' onclick='addPost(" + i + ")' type='submit' value='Add' class='btn-edit btn-primary'/>" +
            "</td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +

            "<table class='table table-bordered'>" +
            "<tbody id='post"+orgAll[i - 1].id +"'>" +
            "</tbody>" +
            "</table>" +

            "</td>" +
            "<td width='15%'>" +
                "<p id='org" + i + "'>"+orgAll[i - 1].org+"</p>" +
            "</td>" +
            "<td width='15%'>" +
            "<p id='balans" + i + "'>"+orgAll[i - 1].balans +"</p>" +
            "</p></td>" +
            "<td>" +

            "<table class='table table-bordered'>" +
            "<tbody>" +
            "<tr class='info'>" +
            "<td>" +
            "<input type='date' value='"+now+"' id='rash" + i + "Data'>" +
            "</td>" +
            "<td>" +
            "<select id='rash" + i + "Operac' onchange='selectOperRash(value,"+i+")'>" +
            "<option value='' label='---Вид операции---'></option>" +
            operacOutHTML +
            "</select>" +
            "</td>" +
            "<td id='orgListRash"+orgAll[i - 1].id+"'>" +
            "</td>" +
            "<td>" +
            "<input id='rash" + i + "Sum' placeholder='Сумма' cssClass='input-sm' cssStyle='width: 60pt'/>" +
            "</td>" +
            "<td>" +
            "<input id='rash" + i + "Koment' placeholder='Комент' cssClass='input-sm' cssStyle='width: 60pt'/>" +
            "</td>" +
            "<td>" +
            "<input id='rash" + i + "Btn' onclick='addRash(" + i + ")' type='submit' value='Add' class='btn-edit btn-primary'/>" +
            "</td>" +
            "</tr>" +
            "</tbody>" +
            "</table>" +

            "<table class='table table-bordered'>" +
            "<tbody id='rash"+orgAll[i - 1].id +"'>" +
            "</tbody>" +
            "</table>"+

        "</td>" +
            "</tr>";
    }
    document.getElementById("tableOrg").innerHTML = tableHTML;
    initPostHTML();
    initRashHTML();

    console.log("end____________________________________");
}

function addPost(i) {


    var date = document.getElementById("post"+i+"Data").value;
    var otkyda = document.getElementById("post"+i+"Otkyda").value;
    var kyda = orgAll[i-1].id+"_org";
    var sumPost = document.getElementById("post"+i+"Sum").value;
    var isp = "0";
    var koment = document.getElementById("post"+i+"Koment").value;
    var vid_operacii = document.getElementById("post"+i+"Operac").value;
    var msgId = "post_add";
    if (otkyda == "" || kyda == "" || sumPost == "") {
        alert("Введите все параметры!")
    } else {
        document.getElementById("post" + i + "Btn").disabled = true;
        stompClient.send("/app/ws_post_add", {}, JSON.stringify({
            date: date,
            otkyda: otkyda,
            kyda: kyda,
            sumPost: sumPost,
            isp: isp,
            koment: koment,
            vid_operacii: vid_operacii,
            msgId: msgId,
            msgCount: i
        }));
    }
}

function addRash(i) {


    var date = document.getElementById("rash"+i+"Data").value;
    var kyda = document.getElementById("rash"+i+"Kyda").value;
    var otkyda = orgAll[i-1].id+"_org";
    var sumPost = document.getElementById("rash"+i+"Sum").value;
    var isp = "0";
    var koment = document.getElementById("rash"+i+"Koment").value;
    var vid_operacii = document.getElementById("rash"+i+"Operac").value;
    var msgId = "post_add";
    if (otkyda == "" || kyda == "" || sumPost == "") {
        alert("Введите все параметры!")
    } else {
        document.getElementById("rash" + i + "Btn").disabled = true;
        stompClient.send("/app/ws_post_add", {}, JSON.stringify({
            date: date,
            otkyda: otkyda,
            kyda: kyda,
            sumPost: sumPost,
            isp: isp,
            koment: koment,
            vid_operacii: vid_operacii,
            msgId: msgId,
            msgCount: i
        }));
    }
}

function initPostHTML() {

    postAll.sort(function (a, b) {
        if (parseInt(a.id) < parseInt(b.id)) {
            return -1;
        }
        if (parseInt(a.id) > parseInt(b.id)) {
            return 1;
        }
        return 0;
    });

    for (i = 1; i <= countOrg; i++) {
        postHTML = "";
        for (j = 0; j <= postAll.length-1; j++) {
            if (postAll[j].kyda == orgAll[i - 1].id + "_org") {
                postHTML = postHTML +
                    "<tr><td>" +
                    postAll[j].date +
                    "</td><td>" +
                    postAll[j].otkyda_str +
                    "</td><td>" +
                    postAll[j].sumPost +
                    "</td><td>";
                if (postAll[j].isp == "1") {
                    postHTML = postHTML +
                        "Исполнено"
                } else {
                    postHTML = postHTML +
                        "Не исполнено"
                }
                postHTML = postHTML +
                    "</td><td>" +
                    "<input id='post" + i + "Koment"+j+"' value='"+postAll[j].koment+"' cssClass='input-sm' cssStyle='width: 60pt'/>" +
                    "</td><td>";
                if (postAll[j].isp == "0") {
                    postHTML = postHTML +
                        "<input id='post" + j + "BtnEx" + i + "' onclick='exPost(" + i + "," + j + ")' type='submit' value='=>' class='btn-edit btn-primary'/>" +
                        "</td><td>" +
                        "<input id='post" + j + "BtnDel" + i + "' onclick='delPost(" + i + "," + j + ")' type='submit' value='X' class='btn-edit btn-danger'/>" +
                        "</td></tr>";
                } else {
                    postHTML = postHTML +
                        "<input id='post" + j + "BtnExBack" + i + "' onclick='exBackPost(" + i + "," + j + ")' type='submit' value='<=' class='btn-edit btn-danger'/>" +
                        "</td><td>" +
                        "<input id='post" + j + "BtnDel" + i + "' onclick='delPost(" + i + "," + j + ")' type='submit' value='X' class='btn-edit btn-danger' disabled='true'/>" +
                        "</td></tr>";

                }
            }
        }
        document.getElementById("post" + i).innerHTML = postHTML;
    }
}

function initRashHTML() {

    postAll.sort(function (a, b) {
        if (parseInt(a.id) < parseInt(b.id)) {
            return -1;
        }
        if (parseInt(a.id) > parseInt(b.id)) {
            return 1;
        }
        return 0;
    });

    for (i = 1; i <= countOrg; i++) {
        rashHTML = "";
        for (j = 0; j <= postAll.length-1; j++) {
            if (postAll[j].otkyda == orgAll[i - 1].id + "_org") {
                rashHTML = rashHTML +
                    "<tr><td>" +
                    postAll[j].date +
                    "</td><td>" +
                    postAll[j].kyda_str +
                    "</td><td>" +
                    postAll[j].sumPost +
                    "</td><td>";
                if (postAll[j].isp == "1") {
                    rashHTML = rashHTML +
                        "Исполнено"
                } else {
                    rashHTML = rashHTML +
                        "Не исполнено"
                }

                rashHTML = rashHTML +
                    "</td><td>" +
                    "<input id='rash" + i + "Koment"+j+"' value='"+postAll[j].koment+"' cssClass='input-sm' cssStyle='width: 60pt'/>" +
                    "</td><td>";
                if (postAll[j].isp == "0") {
                    rashHTML = rashHTML +
                        "<input id='rash" + j + "BtnEx" + i + "' onclick='exRash(" + i + "," + j + ")' type='submit' value='=>' class='btn-edit btn-primary'/>" +
                        "</td><td>" +
                        "<input id='rash" + j + "BtnDel" + i + "' onclick='delRash(" + i + "," + j + ")' type='submit' value='X' class='btn-edit btn-danger'/>" +
                        "</td></tr>";
                } else {
                    rashHTML = rashHTML +
                        "<input id='rash" + j + "BtnExBack" + i + "' onclick='exBackRash(" + i + "," + j + ")' type='submit' value='<=' class='btn-edit btn-danger'/>" +
                        "</td><td>" +
                        "<input id='rash" + j + "BtnDel" + i + "' onclick='delRash(" + i + "," + j + ")' type='submit' value='X' class='btn-edit btn-danger' disabled='true'/>" +
                        "</td></tr>";

                }
            }
        }
        document.getElementById("rash" + i).innerHTML = rashHTML;
    }
}

function delPost(i,j){
    document.getElementById("post" + j + "BtnDel"+i).disabled = true;
    var id = postAll[j].id;
    stompClient.send("/app/ws_post_del", {}, JSON.stringify({  id:id, msgId:i,msgCount:j    }));
}
function delRash(i,j){
    document.getElementById("rash" + j + "BtnDel"+i).disabled = true;
    var id = postAll[j].id;
    stompClient.send("/app/ws_post_del", {}, JSON.stringify({  id:id, msgId:i,msgCount:j    }));
}

function exPost(i,j){
    document.getElementById("post" + j + "BtnEx"+i).disabled = true;
    var koment = document.getElementById("post" + i + "Koment"+j).value;
    var id = postAll[j].id;
    stompClient.send("/app/ws_post_ex", {}, JSON.stringify({  id:id, msgId:i,msgCount:j, koment:koment    }));
}
function exRash(i,j){
    document.getElementById("rash" + j + "BtnEx"+i).disabled = true;
    var koment = document.getElementById("rash" + i + "Koment"+j).value;
    var id = postAll[j].id;
    stompClient.send("/app/ws_post_ex", {}, JSON.stringify({  id:id, msgId:i,msgCount:j, koment:koment    }));
}

function exBackPost(i,j){
    document.getElementById("post" + j + "BtnExBack"+i).disabled = true;
    var koment = document.getElementById("post" + i + "Koment"+j).value;
    var id = postAll[j].id;
    stompClient.send("/app/ws_post_ex_back", {}, JSON.stringify({  id:id, msgId:i,msgCount:j, koment:koment   }));
}
function exBackRash(i,j){
    document.getElementById("rash" + j + "BtnExBack"+i).disabled = true;
    var koment = document.getElementById("rash" + i + "Koment"+j).value;
    var id = postAll[j].id;
    stompClient.send("/app/ws_post_ex_back", {}, JSON.stringify({  id:id, msgId:i,msgCount:j, koment:koment   }));
}

function selectOperPost(value,id){
orgList(id,value,"post");
}
function selectOperRash(value,id){
    orgList(id,value,"rash");
}

function refreshBalans(){
    var id = "refresh_balans";
    stompClient.send("/app/ws_refresh_balans", {}, JSON.stringify({  id:id  }));

}


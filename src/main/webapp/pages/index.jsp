<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Bot for Deck Heroes</title>


  <!-- Bootstrap core CSS -->
  <link href="<c:url value="/pages/css/bootstrap.css" />" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<c:url value="/pages/css/jumbotron-narrow.css" />" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>

<body>

<div class="container">

  <div class="jumbotron" style="margin-top: 20px;">
    <img src="img/dh_pc_logo.png">
    <h1>Bot for Deck Heroes</h1>

      <p>Бот для игры Deck Heroes.</p>
      <p align="left">
        Бот подходит для любой версии версии (английской, немецкой, французкой, русской).<br>
        Скачать версии можно <a href="https://drive.google.com/folderview?id=0B2vBMfX74kavS2Z4NnlLV1FhcXM&usp=sharing">тут</a>.<br>
        За приобритением обращаться на почту 8683007@gmail.com<br>
        Стоимость одного ключа 50$ (один ключ на один компьютер).<br>
        Ключ при активации привязывается к компьютеру, для перепривязки к другому писать мне на почту.<br>
      </p>


  </div>

  <div class="footer">
    <p>© Anonymous 2015</p>
  </div>

</div>
</body>
</html>
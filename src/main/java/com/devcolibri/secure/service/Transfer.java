package com.devcolibri.secure.service;

/**
 * Created by belbix on 22.05.2015.
 */
public class Transfer {

    private String var;

    private String name;

    private String nameOrg;

    private String date_old;

    public String getname() {
        return name;
    }
    public void setname(String name) {
        this.name = name;
    }

    public String getvar() { return var; }
    public void setvar(String var) {
        this.var = var;
    }

    public String getnameOrg() { return nameOrg; }
    public void setnameOrg(String nameOrg) {
        this.nameOrg = nameOrg;
    }

    public String getDate_old() { return date_old; }
    public void setDate_old(String date_old) {
        this.date_old = date_old;
    }
}
